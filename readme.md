# Instalar o plugin ESLINT no react

CTRL + P > Preferences: Open Settings (JSON)

adicionar essas configurações:

```json
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
    },
    "eslint.validate": [
        "javascript",
        "javascriptreact",
        "typescript",
        "typescriptreact"
    ]
```

# Instalar o PRETTIER

```
$ yarn add prettier eslint-config-prettier eslint-plugin-prettier -D
```

## instalar o plugin do vscode:

CTRL + P para abrir a command palette e escrever:

```
ext install esbenp.prettier-vscode
```

Não funcionou a marca de certo no plugin....

# Desenvolver a aplicação

## instalando as dependências

```
$ yarn add express cors mongoose
```

instalar os types para o typescript poder importar os módulos

```
$ yarn add @types/express @types/cors @types/mongoose -D
```
