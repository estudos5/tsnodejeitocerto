import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'

import routes from './routes'

class App {
    public express: express.Application

    public constructor () {
      this.express = express()

      this.middlewares()
      this.database()
      this.routes()
    }

    private middlewares (): void {
      this.express.use(express.json())
      this.express.use(cors())
    }

    private database (): void {
      /* Exemplo do vídeo com conexão ao docker local
      mongoose.connect('mongodb://localhost:27017/tsnode', {
        useNewUrlParser: true
      })
      */

      // igual exemplo da omnistack bootcamp usando o site Mongo Atlas
      mongoose.connect('mongodb+srv://omnistack:omnistack@cluster0-uajaz.mongodb.net/typescript?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true
      })
    }

    private routes (): void {
      this.express.use(routes)
    }
}

export default new App().express
